
Quotes about Org-mode

#+AUTHOR:    
#+EMAIL:     
#+LANGUAGE:  en
#+TEXT:      
#+OPTIONS:   H:3 num:nil toc:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t TeX:t LaTeX:nil skip:nil d:nil tags:not-in-toc author:nil creator:nil
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+LINK_UP:   
#+LINK_HOME: 


* Quotes about Org-mode from the mailing list and the web.
  :PROPERTIES:
  :ID:       70F3B7D4-3EE9-4518-900D-D1D20434F2C2
  :END:

#+begin_quote
I'm having the same feeling for org-mode that I did when I first
learned to really program and use emacs.
@<div align="right">@<i>Jeffery Travis on his [[http://twitter.com/travisjeffery][Twitter feed]].@</i>@</div>
#+end_quote

#+begin_quote
This handy system uses a fairly simple, single-file outlining
paradigm, upon which it overlays concepts like due dates and
priorities. I find its method both non-intrusive and easy to edit
by hand, which are absolute necessities for me.
@<div align="right">@<i>John Wiegley in 2007 in his [[http://www.newartisans.com/blog_files/org.mode.day.planner.php][blog post]].@</i>@</div>
#+end_quote

#+begin_quote
I've passed 2500 tasks logged with Org-mode!
This has been, by far, the most consistently I've used any
organizational tool on any system, ever. :)
@<div align="right">@<i>John Wiegley in 2009 in an Email to the Org-mode author.@</i>@</div>
#+end_quote


#+begin_quote
Someone mentioned that org-mode is a bit like perl. I agree. Way back,
someone described perl as "the Swiss army chainsaw of UNIX
programming". Over the last 12 months, I think org mode has evolved into
something akin to the "Swiss army JCB of organisational software" (to
stretch a metaphor until it screams for mercy!).
@<div align="right">@<i>Pete Phillips in a [[http://article.gmane.org/gmane.emacs.orgmode/754][post on emacs-orgmode]].@</i>@</div>
#+end_quote

#+begin_quote
I think a main reason for [Org-mode's] utility is that basic use
requires little thought. When I'm using it for brainstorming, it's
almost like I'm not aware that I'm using any program --- I'm just
thinking.
@<div align="right">@<i>Someone, in Charles Cave's [[http://orgmode.org/survey.html#sec-11][survey of Org
users]]@</i>@</div>
#+end_quote

#+begin_quote
I'm continually amazed by what org can do, and also by how intuitive
it is.  It's not at all unusual that I find myself thinking that it
would be great if Org/Emacs did "x", trying what seems to me to be the
way that it would do "x" if it could, and discovering that it
functions just as I expect.
@<div align="right">@<i>Someone, in Charles Cave's [[http://orgmode.org/survey.html#sec-11][survey of Org users]]@</i>@</div>
#+end_quote

#+begin_quote
I love Org's timeclocking support, and I think you will too. Because
it's integrated with your task list, you don't have to switch to
separate application or reenter data.
@<div align="right">@<i>Sacha Chua in a [[http://sachachua.com/wp/2007/12/30/clocking-time-with-emacs-org][blog post]].@</i>@</div>
#+end_quote

#+begin_quote
I've been trying lots of different Web-based GTD task managers like
Remember the Milk, Toodledo, and GTDAgenda.  I'm slowly coming to the
conclusion that there's nothing quite like Org for Emacs.
@<div align="right">@<i>Sacha Chua in a [[http://sachachua.com/wp/2009/04/06/nothing-quite-like-org-for-emacs/][blog post]].@</i>@</div>
#+end_quote

#+begin_quote
By far my favorite featureset in org-mode that muse lacks[fn:1] is the table
support, which piggybacks on calc to form more of a spreadsheet than
table support. Insanely cool.
@<div align="right">@<i>Patrick Hawkins in a [[http://article.gmane.org/gmane.emacs.wiki.general/5760][post on emacs-wiki-discuss]].@</i>@</div>
#+end_quote

#+begin_quote
Org is a new working experience for me and there is nothing comparable
to working with emacs AND Org-mode.
@<div align="right">@<i>Sebastian Rose in his [[http://orgmode.org/worg/code/org-info-js/][org-info.js documentation]].@</i>@</div>
#+end_quote

#+begin_quote
Org-mode definition:\\
Org-mode is an emacs mode for doing anything you dream of. If it
can't do it yet, post a message on the mailing list at night, go for
a sleep, and grab in the morning a fresh copy with your features
implemented.
@<div align="right">@<i>Paul Rivier in an email message to the
Org-mode author.@</i>@</div>
#+end_quote

#+begin_quote
[...] Org-mode [...] continues to amaze me with its power and
utility each and every day.
@<div align="right">@<i>Bernt Hansen in a [[http://thread.gmane.org/gmane.emacs.orgmode/9213][post on emacs-orgmode]].@</i>@</div>
#+end_quote

#+begin_quote
PT>   Damn! Org is again a step ahead of me. :D\\
Nick> Yup - get used to it ;-)
@<div align="right">@<i>PT and Nick Dokos [[http://thread.gmane.org/gmane.emacs.orgmode/17130/focus%3D17156][on emacs-orgmode]].@</i>@</div>
#+end_quote

#+begin_quote
Org-mode has changed my life!
@<div align="right">@<i>Jonathan E. Magen in a [[http://yonkeltron.com/2008/11/10/org-mode-has-changed-my-life/][blog post]]@</i>@</div>
#+end_quote

#+begin_quote
If humans could mate with software, I'd have org-mode's babies.
@<div align="right">@<i>Chris League on his [[http://twitter.com/chrisleague][Twitter feed]].@</i>@</div>
#+end_quote


#+begin_quote
If I hated everything about Emacs, I would still use it for
org-mode.  It's that good.
@<div align="right">@<i>Avdi on his [[http://twitter.com/avdi][Twitter feed]]@</i>@</div>
#+end_quote

#+begin_quote
If Emacs is an operating system, Org-mode is the office/productivity
suite. 
@<div align="right">@<i>Eric Schulte in his [[http://orgmode.org/worg/images/screenshots/org-mode-publishing.jpg][screenshot]] on [[http://orgmode.org/worg/][Worg]]@</i>@</div>
#+end_quote

##+begin_quote
#I think I understand the difference between /org-mode/ and /planner.el/
#now.  The former is more like an outline with dates and hypertext and
#lots of other features, while the latter is more like a schedule with
#outlines and hypertext and lots of other features.\\
#@<div align="right">@<i>Samuel Wales in [[http://thread.gmane.org/gmane.emacs.planner.general/1279/focus%3D1283][a post on the planner mailing list]]@</i>@</div>
##+end_quote

#+begin_quote
Org-mode seemed like a way to tame the text file beast and ride
it off into the sunset.\\
@<div align="right">@<i>Joey Doll in a [[http://www.guyslikedolls.com/set-phasers-to-org-mode][blog post]]@</i>@</div>
#+end_quote

#+begin_quote
I have no idea how long [these files] are, probably 1000
lines each, but it doesn't matter. I can combine long winded notes
about my latest fabrication process with that thing that I have to do
on it next week, fold everything back up, and then keep easy tabs on
everything using the agenda view.
@<div align="right">@<i>Joey Doll in a [[http://www.guyslikedolls.com/set-phasers-to-org-mode][blog post]]@</i>@</div>
#+end_quote

#+begin_quote
Org-mode is a note taking tool unparalleled in it's simplicity and
ease of use.\\
@<div align="right">@<i>Shrutarshi Basu in a [[http://bytebaker.com/2009/06/23/too-many-formats/][blog post]]@</i>@</div>
#+end_quote


* Some 24/7 lectures about Org-mode

The famous 24/7 lectures are part of the ceremony for handing out the
[[http://en.wikipedia.org/wiki/Ig_Nobel_Prize][Ig Nobel Prizes]].  All speakers have to give a 24/7 lecture on their
subject. This means, they have to give a /complete technical
description/ of their work in /24 words/ (may be totally cryptic), and
then a /7 word/ explanation that is more or less /understandable for
the public/, and it may be either tongue in cheek or serious.  In
summer 2008, a few people tried to [[http://thread.gmane.org/gmane.emacs.orgmode/7599][formulate]] such lectures about
Org-mode:

** Technical description in 24 words

These was only a single entry in the "24" category:

  - Org-mode does outlining, note-taking, hyperlinks, spreadsheets,
    TODO lists, project planning, GTD, HTML and LaTeX authoring, all
    with plain text files in Emacs (/Carsten Dominik/)


** Simple summary in 7 words

   This is only a selection of the submitted entries.  My loose
   criterion was to use entries that are either a good description or
   are funny - both valid approaches to the "7" part of 24/7 lectures.
   I also left a few entries which are not exactly seven words,
   because I liked them a lot.

   - Organize and track everything in plain text (/Bernt Hansen/)

   - Organize outlines, lists and table in text. (/Eddward DeVilla/)

   - Emacs Org Mode: your life in text (/Matthew Parker/)

   - Do work and play in plain text (/Kene Meniru/)

   - Madness? This is org-mode! [[http://www.youtube.com/watch%3Fv%3DUgrsNBu51nU][*Real Spartans use emacs!*]] (/Russell
     Adams[fn:2]/)
     
   - Plain text with frickin' lasers. [[http://en.wikipedia.org/wiki/Dr._Evil][*pinky to lips*]] (/Russell Adams[fn:2]/)

   - It is the text that binds us. [[http://www.urbandictionary.com/define.php%3Fterm%3Dshikaka][*Shekaka!*]] (/Russell Adams[fn:2]/)
     

   - Org-mode --- lifehacker's orgy :-P (/Dmitry Dzhus/)

   - Back to the future for plain text (/Carsten Dominik/)

Footnotes: 

[fn:1] Muse now understands the syntax of Org-mode tables, so you can use
Orgtbl-mode to get the same tables in Muse.

[fn:2] The linked text is from Adam, but the link itself has been added
by me.


